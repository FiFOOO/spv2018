package main;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;


public class Game extends Application {
    private Color selected_color;
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(800, 600);
        BorderPane bp = new BorderPane(canvas);
        selected_color = Color.BLUE;

//        Ball ball = new Ball();
//        ball.paint();
//
//        House house = new House();
//        house.paint();
//
        Flag flag = new Flag();
        flag.paint();

//        bp.getChildren().add(ball);
//        bp.getChildren().addAll(house.triangle, house.rectangle);
        bp.getChildren().addAll(flag.rectangleBottom, flag.rectangleMiddle, flag.rectangleTop);
        Scene scene = new Scene(new Group(bp), 800, 600);
        primaryStage.setTitle("JavaFX-Vyfarbovanie");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    public static void main(String[] args) {
        launch(args);
    }

    class Ball extends Circle {
        String text = "Najviac koľko rôznych lôpt vieš vytvoriť tak, že ani jeden nebude vyfarbený rovnako ako ostatné?";

        void paint() {
            this.setOnMouseClicked(event -> this.setFill(selected_color));
            this.setStroke(Color.BLACK);
            this.setFill(Color.WHITE);
            this.setCenterX(100);
            this.setCenterY(100);
            this.setRadius(50);
        }
    }

    class House {
        String text = "Najviac koľko rôznych domčekov vieš vytvoriť tak, že ani jeden nebude vyfarbený rovnako ako ostatné?";
        Polygon triangle = new Polygon();
        Rectangle rectangle = new Rectangle();

        void paint() {
            triangle.setOnMouseClicked(event -> triangle.setFill(selected_color));
            rectangle.setOnMouseClicked(event -> rectangle.setFill(selected_color));
            triangle.getPoints().setAll(
                    100d, 100d,
                    150d, 50d,
                    200d, 100d
            );
            triangle.setStroke(Color.BLACK);
            triangle.setFill(Color.WHITE);

            rectangle.setX(110);
            rectangle.setY(100);
            rectangle.setHeight(50);
            rectangle.setWidth(80);
            rectangle.setStroke(Color.BLACK);
            rectangle.setFill(Color.WHITE);
        }
    }

    class Flag {
        String text = "Najviac koľko rôznych vlajok vieš vytvoriť tak, že ani jeden nebude vyfarbený rovnako ako ostatné?";
        Rectangle rectangleTop = new Rectangle(100, 100, 150, 30);
        Rectangle rectangleMiddle = new Rectangle(100, 130, 150, 30);
        Rectangle rectangleBottom = new Rectangle(100, 160, 150, 30);

        void paint() {
            rectangleTop.setOnMouseClicked(event -> rectangleTop.setFill(selected_color));
            rectangleMiddle.setOnMouseClicked(event -> rectangleMiddle.setFill(selected_color));
            rectangleBottom.setOnMouseClicked(event -> rectangleBottom.setFill(selected_color));

            rectangleTop.setStroke(Color.BLACK);
            rectangleTop.setFill(Color.WHITE);

            rectangleMiddle.setStroke(Color.BLACK);
            rectangleMiddle.setFill(Color.WHITE);

            rectangleBottom.setStroke(Color.BLACK);
            rectangleBottom.setFill(Color.WHITE);
        }
    }
}